// Declarative //
pipeline {
    agent any
    environment {
        RHOCP_CREDENTIALS = credentials('rhocp-web-team-dev-sa')
        RHOCP_CLUSTER = 'https://api.lab.ocp.lan:6443'
        RHOCP_PROJECT = 'web-team-dev'
        APP_REPOSITORY = 'https://bitbucket.org/bankierubybank/first-bitbucket-repo/src/dev/'
        APP_NAME = 'node-express-demo'
    }
    tools {
        //Configured OpenShift Client Tools on Jenkins Global Tool Configuration
        oc 'oc-latest' //Configured OpenShift Client Tools name
    }
    stages {
        stage('PREAMBLE') {
            steps {
                sh 'oc login --server=${RHOCP_CLUSTER} --insecure-skip-tls-verify --token=${RHOCP_CREDENTIALS}'
                sh 'oc project ${RHOCP_PROJECT}'
            }
        }
        stage('NEW APP') {
            steps {
                script {
                    //Returns true if not found (err)
                    def tmp = sh(returnStatus: true, script: 'oc -n ${RHOCP_PROJECT} get deployment/${APP_NAME}')
                    if (!tmp) {
                        echo "Skipped as already have application deployment"
                        sh 'oc start-build ${APP_NAME}'
                    } else {
                        echo "Creating new application"
                        sh 'oc new-app --name ${APP_NAME} ${APP_REPOSITORY}'
                    }
                }
            }
        }
        stage('EXPOSE ROUTE') {
            steps {
                script {
                    //Returns true if not found (err)
                    def tmp = sh(returnStatus: true, script: 'oc -n ${RHOCP_PROJECT} get routes/${APP_NAME}')
                    if (!tmp) {
                        echo "Skipped as already exposed route"
                    } else {
                        echo "Exposing route"
                        sh 'oc expose svc/${APP_NAME} --name=${APP_NAME}'
                    }
                }
            }
        }
    }
}