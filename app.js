const express = require('express');
const router = require('/usr/src/app/routes/router.js');

//Create API server on port 8080
async function createServer() {
	const app = express();

	//Routes defined on router.js file
	app.use('/', router);

	let server = await app.listen(8080, () => console.log('App listen on port: 8080'));
}

createServer();